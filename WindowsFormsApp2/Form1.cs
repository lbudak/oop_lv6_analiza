﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        List<string> list = new List<string>();
        string path = "vješala.txt";
        int counter = 7;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    list.Add(line);
                listBox1.DataSource = null;
                listBox1.DataSource = list;
                listBox1.Hide();

            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            string word = listBox1.SelectedItem.ToString();
            string letter = textBox1.Text;

            if (word.Contains(letter))
            {
                label2.Text = "";
                for (int j = 0; j < word.Length; j++) {
                    if (word.Substring(j, 1) == letter)
                        label2.Text += letter[0] + " ";
                    else
                        label2.Text += "_ ";
                }
                    //word.Substring(0, word.Length - 1);

            }
            else
            {
                label1.Text = (--counter).ToString();
                if (counter == 0)
                {
                    MessageBox.Show("Izgubili ste, nisi pogodio " + listBox1.SelectedItem.ToString());
                    counter = 7;
                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            Random generator = new Random();
            int var = listBox1.Items.Count;
            int i = generator.Next(0, var);
            listBox1.SetSelected(i, true);
            string a = listBox1.SelectedItem.ToString();
            label2.Text = "";
            for (int j = 0; j < a.Length; j++)
            {
                label2.Text = label2.Text + "_ ";
            }
            label1.Text = counter.ToString();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string a = listBox1.SelectedItem.ToString();
            string b = textBox2.Text;
            if (a == b)
                MessageBox.Show("Pogodili ste");
            else
                MessageBox.Show("FAIL!");
            

        }


    }
}
