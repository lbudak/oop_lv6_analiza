﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button0 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.buttontocka = new System.Windows.Forms.Button();
            this.buttonplus = new System.Windows.Forms.Button();
            this.buttonjednako = new System.Windows.Forms.Button();
            this.buttonputa = new System.Windows.Forms.Button();
            this.buttondijeljenjo = new System.Windows.Forms.Button();
            this.buttonminus = new System.Windows.Forms.Button();
            this.buttonclear = new System.Windows.Forms.Button();
            this.buttonkorjen = new System.Windows.Forms.Button();
            this.buttoncos = new System.Windows.Forms.Button();
            this.buttonsin = new System.Windows.Forms.Button();
            this.buttontan = new System.Windows.Forms.Button();
            this.buttonlog = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(372, 121);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(82, 20);
            this.textBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(129, 248);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(75, 23);
            this.button0.TabIndex = 2;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(48, 219);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(129, 219);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(210, 219);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(48, 190);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(129, 190);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 7;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(210, 190);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 8;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(48, 161);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 9;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(129, 161);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 10;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(210, 161);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 11;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // buttontocka
            // 
            this.buttontocka.Location = new System.Drawing.Point(210, 248);
            this.buttontocka.Name = "buttontocka";
            this.buttontocka.Size = new System.Drawing.Size(75, 23);
            this.buttontocka.TabIndex = 12;
            this.buttontocka.Text = ".";
            this.buttontocka.UseVisualStyleBackColor = true;
            this.buttontocka.Click += new System.EventHandler(this.buttontocka_Click);
            // 
            // buttonplus
            // 
            this.buttonplus.Location = new System.Drawing.Point(291, 161);
            this.buttonplus.Name = "buttonplus";
            this.buttonplus.Size = new System.Drawing.Size(75, 23);
            this.buttonplus.TabIndex = 13;
            this.buttonplus.Text = "+";
            this.buttonplus.UseVisualStyleBackColor = true;
            this.buttonplus.Click += new System.EventHandler(this.buttonplus_Click);
            // 
            // buttonjednako
            // 
            this.buttonjednako.Location = new System.Drawing.Point(291, 118);
            this.buttonjednako.Name = "buttonjednako";
            this.buttonjednako.Size = new System.Drawing.Size(75, 23);
            this.buttonjednako.TabIndex = 14;
            this.buttonjednako.Text = "=";
            this.buttonjednako.UseVisualStyleBackColor = true;
            this.buttonjednako.Click += new System.EventHandler(this.buttonjednako_Click);
            // 
            // buttonputa
            // 
            this.buttonputa.Location = new System.Drawing.Point(291, 219);
            this.buttonputa.Name = "buttonputa";
            this.buttonputa.Size = new System.Drawing.Size(75, 23);
            this.buttonputa.TabIndex = 15;
            this.buttonputa.Text = "*";
            this.buttonputa.UseVisualStyleBackColor = true;
            this.buttonputa.Click += new System.EventHandler(this.buttonputa_Click);
            // 
            // buttondijeljenjo
            // 
            this.buttondijeljenjo.Location = new System.Drawing.Point(291, 248);
            this.buttondijeljenjo.Name = "buttondijeljenjo";
            this.buttondijeljenjo.Size = new System.Drawing.Size(75, 23);
            this.buttondijeljenjo.TabIndex = 16;
            this.buttondijeljenjo.Text = "/";
            this.buttondijeljenjo.UseVisualStyleBackColor = true;
            this.buttondijeljenjo.Click += new System.EventHandler(this.buttondijeljenjo_Click);
            // 
            // buttonminus
            // 
            this.buttonminus.Location = new System.Drawing.Point(291, 190);
            this.buttonminus.Name = "buttonminus";
            this.buttonminus.Size = new System.Drawing.Size(75, 23);
            this.buttonminus.TabIndex = 17;
            this.buttonminus.Text = "-";
            this.buttonminus.UseVisualStyleBackColor = true;
            this.buttonminus.Click += new System.EventHandler(this.buttonminus_Click);
            // 
            // buttonclear
            // 
            this.buttonclear.Location = new System.Drawing.Point(48, 248);
            this.buttonclear.Name = "buttonclear";
            this.buttonclear.Size = new System.Drawing.Size(75, 23);
            this.buttonclear.TabIndex = 18;
            this.buttonclear.Text = "CE";
            this.buttonclear.UseVisualStyleBackColor = true;
            this.buttonclear.Click += new System.EventHandler(this.buttonclear_Click);
            // 
            // buttonkorjen
            // 
            this.buttonkorjen.Location = new System.Drawing.Point(372, 161);
            this.buttonkorjen.Name = "buttonkorjen";
            this.buttonkorjen.Size = new System.Drawing.Size(75, 23);
            this.buttonkorjen.TabIndex = 19;
            this.buttonkorjen.Text = "korjen";
            this.buttonkorjen.UseVisualStyleBackColor = true;
            this.buttonkorjen.Click += new System.EventHandler(this.buttonkorjen_Click);
            // 
            // buttoncos
            // 
            this.buttoncos.Location = new System.Drawing.Point(372, 190);
            this.buttoncos.Name = "buttoncos";
            this.buttoncos.Size = new System.Drawing.Size(75, 23);
            this.buttoncos.TabIndex = 20;
            this.buttoncos.Text = "cos";
            this.buttoncos.UseVisualStyleBackColor = true;
            this.buttoncos.Click += new System.EventHandler(this.buttoncos_Click);
            // 
            // buttonsin
            // 
            this.buttonsin.Location = new System.Drawing.Point(372, 219);
            this.buttonsin.Name = "buttonsin";
            this.buttonsin.Size = new System.Drawing.Size(75, 23);
            this.buttonsin.TabIndex = 21;
            this.buttonsin.Text = "sin";
            this.buttonsin.UseVisualStyleBackColor = true;
            this.buttonsin.Click += new System.EventHandler(this.buttonsin_Click);
            // 
            // buttontan
            // 
            this.buttontan.Location = new System.Drawing.Point(372, 248);
            this.buttontan.Name = "buttontan";
            this.buttontan.Size = new System.Drawing.Size(75, 23);
            this.buttontan.TabIndex = 22;
            this.buttontan.Text = "tan";
            this.buttontan.UseVisualStyleBackColor = true;
            this.buttontan.Click += new System.EventHandler(this.buttontan_Click);
            // 
            // buttonlog
            // 
            this.buttonlog.Location = new System.Drawing.Point(453, 207);
            this.buttonlog.Name = "buttonlog";
            this.buttonlog.Size = new System.Drawing.Size(75, 23);
            this.buttonlog.TabIndex = 23;
            this.buttonlog.Text = "log";
            this.buttonlog.UseVisualStyleBackColor = true;
            this.buttonlog.Click += new System.EventHandler(this.buttonlog_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonlog);
            this.Controls.Add(this.buttontan);
            this.Controls.Add(this.buttonsin);
            this.Controls.Add(this.buttoncos);
            this.Controls.Add(this.buttonkorjen);
            this.Controls.Add(this.buttonclear);
            this.Controls.Add(this.buttonminus);
            this.Controls.Add(this.buttondijeljenjo);
            this.Controls.Add(this.buttonputa);
            this.Controls.Add(this.buttonjednako);
            this.Controls.Add(this.buttonplus);
            this.Controls.Add(this.buttontocka);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button buttontocka;
        private System.Windows.Forms.Button buttonplus;
        private System.Windows.Forms.Button buttonjednako;
        private System.Windows.Forms.Button buttonputa;
        private System.Windows.Forms.Button buttondijeljenjo;
        private System.Windows.Forms.Button buttonminus;
        private System.Windows.Forms.Button buttonclear;
        private System.Windows.Forms.Button buttonkorjen;
        private System.Windows.Forms.Button buttoncos;
        private System.Windows.Forms.Button buttonsin;
        private System.Windows.Forms.Button buttontan;
        private System.Windows.Forms.Button buttonlog;
    }
}

